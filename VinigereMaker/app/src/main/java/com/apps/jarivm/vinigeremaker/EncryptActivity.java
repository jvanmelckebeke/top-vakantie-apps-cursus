package com.apps.jarivm.vinigeremaker;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by Jari Van Melckebeke on 25.07.17.
 *
 * @since 25.07.17
 */

public class EncryptActivity extends AppCompatActivity {

    static String alphabet = "0123456789aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ-/ \\\'\"";

    public static String encode(String key, String toEncode) {
        int iOfKey = 0;
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < toEncode.length(); i++) {
            char cToEncode = toEncode.charAt(i);
            int indexOfOrigChar = alphabet.indexOf(cToEncode);
            int indexOfKeyChar = alphabet.indexOf(key.charAt(iOfKey));
            int alphabetnumber = ((indexOfKeyChar + 2 + indexOfOrigChar) - 1) % alphabet.length();
            out.append(alphabet.charAt(alphabetnumber));
            iOfKey++;
            iOfKey %= key.length();
        }
        return out.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encode);
        final EditText textToEncode = (EditText) findViewById(R.id.decoded);
        final EditText keyText = (EditText) findViewById(R.id.keyEditText);
        final TextView encoded = (TextView) findViewById(R.id.encoded);
//        final String key = "LOREM IPSUM DOLOR SIT AMET";
        keyText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String key = charSequence.toString();
                if (key.equals("")) {
                    //generate new key
                    String newKey = "";
                    for (int j = 0; j < 8; j++) {
                        int ran = new Random().nextInt(alphabet.length());
                        newKey += alphabet.charAt(ran);
                    }
                    keyText.setText(newKey);
                    key = newKey;
                }
                encoded.setText(encode(key, textToEncode.getText().toString()));

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        textToEncode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.equals(""))
                    encoded.setText(encode(keyText.getText().toString(), charSequence.toString()));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}
