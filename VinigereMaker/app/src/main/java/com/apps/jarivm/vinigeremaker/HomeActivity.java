package com.apps.jarivm.vinigeremaker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button btnEncrypt = (Button) findViewById(R.id.encryptbutton);
        Button btnDecrypt = (Button) findViewById(R.id.decryptbutton);

        btnEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextView = new Intent(getApplicationContext(), EncryptActivity.class);
                startActivity(nextView);
            }
        });

        btnDecrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextview = new Intent(getApplicationContext(), DecryptActivity.class);
                startActivity(nextview);
            }
        });

    }
}
