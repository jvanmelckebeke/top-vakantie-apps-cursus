package com.apps.jarivm.vinigeremaker;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by Jari Van Melckebeke on 25.07.17.
 *
 * @since 25.07.17
 */

public class DecryptActivity extends AppCompatActivity {
    static String alphabet = "0123456789aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ-/ \\\'\"";

    private String decode(String key, String textToDecode) {
        int iOfKey = 0;
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < textToDecode.length(); i++) {
            char cToEncode = textToDecode.charAt(i);
            int indexOfOrigChar = alphabet.indexOf(cToEncode) + 1;
            int indexOfKeyChar = alphabet.indexOf(key.charAt(iOfKey));
            int alphabetnumber = ((indexOfKeyChar + 1 + indexOfOrigChar) ) % alphabet.length();
            out.append(alphabet.charAt(alphabetnumber));
            iOfKey++;
            iOfKey %= key.length();
        }
        return out.toString();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decrypt);
        final EditText toDecodeEditText = (EditText) findViewById(R.id.toDecodeField);
        final EditText key = (EditText) findViewById(R.id.keyfield);
        final TextView tv = (TextView) findViewById(R.id.decodedTextField);

        key.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String tKey = charSequence.toString();
                if (tKey.equals("")) {
                    //generate new key
                    String newKey = "";
                    for (int j = 0; j < 8; j++) {
                        int ran = new Random().nextInt(alphabet.length());
                        newKey += alphabet.charAt(ran);
                    }
                    key.setText(newKey);
                    tKey = newKey;
                }
                tv.setText(decode(tKey, toDecodeEditText.getText().toString()));

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        toDecodeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.equals(""))
                    tv.setText(decode(key.getText().toString(), charSequence.toString()));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}
