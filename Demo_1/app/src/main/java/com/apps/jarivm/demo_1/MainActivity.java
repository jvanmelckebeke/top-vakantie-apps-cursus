package com.apps.jarivm.demo_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NumberPicker np = (NumberPicker) findViewById(R.id.numbers);
        np.setValue(0);
        np.setMaxValue(1000);
        np.setMinValue(0);
        np.setWrapSelectorWheel(true);
        final TextView tv = (TextView) findViewById(R.id.hw);
        CheckBox cb = (CheckBox) findViewById(R.id.magicbutton);

        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                int checkornot = (b) ? View.VISIBLE : View.INVISIBLE;
                tv.setVisibility(checkornot);
            }
        });
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
                public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                tv.setY(i1);
            }
        });
    }
}
