package com.apps.jarivm.scheduler.models;

import com.framgia.library.calendardayview.data.IEvent;

import java.util.Calendar;

/**
 * Created by Jari Van Melckebeke on 16.07.17.
 *
 * @since 16.07.17
 */
public class Event implements IEvent {
    private String name;
    private int color;
    private Calendar startTime, endTime;
    private String location;
    private long id;

    public Event(String name, int color, Calendar startTime, Calendar endTime, String location, long id) {
        this.name = name;
        this.color = color;
        this.startTime = startTime;
        this.endTime = endTime;
        this.location = location;
        this.id = id;
    }

    public Event(String name, int color, Calendar startTime, Calendar endTime) {
        this.name = name;
        this.color = color;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Event() {
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public Calendar getStartTime() {
        return startTime;
    }

    public void setStartTime(Calendar startTime) {
        this.startTime = startTime;
    }

    @Override
    public Calendar getEndTime() {
        return endTime;
    }

    public void setEndTime(Calendar endTime) {
        this.endTime = endTime;
    }
}
